package com.example.examenandroid.data.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.examenandroid.data.models.Empleados;

import java.util.ArrayList;
import java.util.List;


public class EmpleadosHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "examen.db";
    public static final String TABLE_NAME = "empleados";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_BIRTH_DAY = "birt_date";
    public static final String COLUMN_OCCUPATION = "occupation";


    public EmpleadosHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+TABLE_NAME+" (id text primary key, name text, birt_date text,occupation text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public boolean saveEmpleados (List<Empleados> empleadosList) {
        for (Empleados empleado : empleadosList){
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", empleado.getName());
            contentValues.put("name", empleado.getName());
            contentValues.put("birt_date", empleado.getBirthDate());
            contentValues.put("occupation", empleado.getOccupation());
            db.insert(TABLE_NAME, null, contentValues);
        }

        return true;
    }

    public List<Empleados> getEmpleados() {
        List<Empleados> modEmpleados = new ArrayList<Empleados>();
        int count=0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_NAME, null );
        res.moveToFirst();
        while(res.isAfterLast() == false){

            Empleados employee = new Empleados(
                    res.getString(res.getColumnIndex(COLUMN_NAME)),
                    res.getString(res.getColumnIndex(COLUMN_BIRTH_DAY)),
                    res.getString(res.getColumnIndex(COLUMN_OCCUPATION))

            );
            modEmpleados.add(count,employee);
            res.moveToNext();

        }
        return modEmpleados;
    }
}