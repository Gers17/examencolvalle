package com.example.examenandroid.data.models;

public class Empleados {
    private String name;
    private String birthDate;
    private String occupation;

    public Empleados(String name, String birthDate, String occupation) {
        this.name = name;
        this.birthDate = birthDate;
        this.occupation = occupation;
    }

    public String getName() {
        return name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
}
