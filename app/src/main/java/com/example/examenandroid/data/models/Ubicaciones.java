package com.example.examenandroid.data.models;

public class Ubicaciones {

    private int code;
    private boolean hasError;
    private String valueResponse;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public String getValueResponse() {
        return valueResponse;
    }

    public void setValueResponse(String valueResponse) {
        this.valueResponse = valueResponse;
    }
}
