package com.example.examenandroid.data.network;

import com.example.examenandroid.data.models.Ubicaciones;
import com.example.examenandroid.data.models.request.RequestUbicaciones;
import com.google.gson.JsonObject;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface NetwokAPI {

    @POST("generic/getData")
    Observable<Ubicaciones> getUbicaciones(
            @Body RequestUbicaciones requestUbicaciones
    );
}