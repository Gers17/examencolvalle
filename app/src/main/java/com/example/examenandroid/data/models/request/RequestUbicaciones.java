package com.example.examenandroid.data.models.request;

public class RequestUbicaciones {

    private  int serviceId;
    private int userId;

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
