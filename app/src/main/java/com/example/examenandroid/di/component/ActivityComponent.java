package com.example.examenandroid.di.component;


import com.example.examenandroid.di.module.ActivityModule;
import com.example.examenandroid.di.module.NetworkModule;
import com.example.examenandroid.ui.empleados.EmpleadosActivity;
import com.example.examenandroid.ui.map.MapActivity;
import com.example.examenandroid.ui.thread.ThreadActivity;
import com.example.examenandroid.ui.webservice.WebServiceActivity;
import com.example.examenandroid.ui.webservice.WebServiceMVP;

import dagger.Component;

@Component( modules = {ActivityModule.class, NetworkModule.class})
public interface ActivityComponent {

    void inject(MapActivity activity);
    void inject(WebServiceActivity activity);

    void inject(ThreadActivity activity);
    void inject(EmpleadosActivity activity);
}
