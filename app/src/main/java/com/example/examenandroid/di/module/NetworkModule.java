package com.example.examenandroid.di.module;

import com.example.examenandroid.data.network.DownloadService;
import com.example.examenandroid.data.network.NetwokAPI;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private final String BASE_URL = "http://upaxdev.com/ws/webresources/";
    @Provides
    OkHttpClient provideHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build();
    }

    @Provides
    Retrofit provideRetrofit(String baseUrl, OkHttpClient client, boolean isStreamingService) {

        if (isStreamingService) {
            return new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();
        } else {
            return new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
    }


    @Provides
    NetwokAPI provideNetwokAPI() {
        return provideRetrofit(BASE_URL, provideHttpClient(), false).create(NetwokAPI.class);
    }

    @Provides
    DownloadService provideFileDownloadService() {
        return provideRetrofit(BASE_URL, provideHttpClient(), true).create(DownloadService.class);
    }

}