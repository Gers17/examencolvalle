package com.example.examenandroid.di.module;

import android.content.Context;

import com.example.examenandroid.data.db.EmpleadosHelper;
import com.example.examenandroid.data.models.Empleados;
import com.example.examenandroid.data.network.DownloadService;
import com.example.examenandroid.data.network.NetwokAPI;
import com.example.examenandroid.ui.empleados.EmpleadosMVP;
import com.example.examenandroid.ui.empleados.EmpleadosModel;
import com.example.examenandroid.ui.empleados.EmpleadosPresenter;
import com.example.examenandroid.ui.empleados.adapter.EmpleadosAdapter;
import com.example.examenandroid.ui.map.MapMVP;
import com.example.examenandroid.ui.map.MapPresenter;
import com.example.examenandroid.ui.thread.ThreadMVP;
import com.example.examenandroid.ui.thread.ThreadPresenter;
import com.example.examenandroid.ui.webservice.WebServiceMVP;
import com.example.examenandroid.ui.webservice.WebServiceModel;
import com.example.examenandroid.ui.webservice.WebServicePresenter;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private Context context;

    public ActivityModule(Context context){
        this.context = context;
    }

    @Provides
    public Context provideContext(){
        return context;
    }

    /* Mapa */
    @Provides
    MapMVP.Presenter provideMapPresenter(Context context){
        return new MapPresenter(context);
    }

    /*WebService*/
    @Provides
    WebServiceMVP.Model provideWebServiceModel(NetwokAPI netwokAPI, DownloadService downloadService){
        return new WebServiceModel(netwokAPI, downloadService);
    }

    @Provides
    WebServiceMVP.Presenter provideWebServicePresenter(WebServiceMVP.Model model, Context context){
        return new WebServicePresenter(model,context);
    }

    /* Tarea en Segundo Hilo */

    @Provides
    ThreadMVP.Presenter provideThreadPresenter(Context context){
        return  new ThreadPresenter(context);
    }

    /* Empleados */
    @Provides
    EmpleadosHelper provideEmployeesOpenHelper(Context context){
        return new EmpleadosHelper(context);
    }

    @Provides
    EmpleadosMVP.Presenter provideEmployeePresenter(EmpleadosMVP.Model model){
        return new EmpleadosPresenter(model);
    }

    @Provides
    EmpleadosMVP.Model provideEmployeModel(EmpleadosHelper empleadosHelper){
        return new EmpleadosModel(empleadosHelper);
    }



    @Provides
    EmpleadosAdapter provideEmployeesAdapter(){
        return new EmpleadosAdapter(new ArrayList<Empleados>());
    }




}
