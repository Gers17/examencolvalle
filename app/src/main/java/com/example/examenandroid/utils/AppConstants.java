package com.example.examenandroid.utils;

public class AppConstants {

    public static final int RADIUS_MARKERS=600;
    public static final float ZOOM_MAP = 12f;
    public static final String ZIP = "archivoRespuesta.zip";

}
