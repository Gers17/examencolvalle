package com.example.examenandroid.utils;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import okhttp3.ResponseBody;

import static android.content.ContentValues.TAG;

public class AppUtils {

    public static LatLng getRandomLocation(LatLng point, int radius) {

        try {
            List<LatLng> randomPoints = new ArrayList<>();
            List<Float> randomDistances = new ArrayList<>();
            Location myLocation = new Location("");
            myLocation.setLatitude(point.latitude);
            myLocation.setLongitude(point.longitude);

            //This is to generate 10 random points
            for(int i = 0; i<10; i++) {
                double x0 = point.latitude;
                double y0 = point.longitude;

                Random random = new Random();

                // Convert radius from meters to degrees
                double radiusInDegrees = radius / 111000f;

                double u = random.nextDouble();
                double v = random.nextDouble();
                double w = radiusInDegrees * Math.sqrt(u);
                double t = 2 * Math.PI * v;
                double x = w * Math.cos(t);
                double y = w * Math.sin(t);

                // Adjust the x-coordinate for the shrinking of the east-west distances
                double new_x = x / Math.cos(y0);

                double foundLatitude = new_x + x0;
                double foundLongitude = y + y0;
                LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);
                randomPoints.add(randomLatLng);
                Location l1 = new Location("");
                l1.setLatitude(randomLatLng.latitude);
                l1.setLongitude(randomLatLng.longitude);
                randomDistances.add(l1.distanceTo(myLocation));
            }
            //Get nearest point to the centre
            int indexOfNearestPointToCentre = randomDistances.indexOf(Collections.min(randomDistances));
            return randomPoints.get(indexOfNearestPointToCentre);
        }catch (Exception e){
            e.printStackTrace();
            return point;
        }

    }

    public static File writeResponseBodyToDisk(ResponseBody body, Context context, String fileName) {
        try {

            File file = new File(context.getFilesDir()+File.separator+"app_upax/"+fileName);
            File directory = new File(context.getFilesDir()+File.separator+"app_upax");

            if(!directory.exists()) {
                directory.mkdir();
            }

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return file;
            } catch (IOException e) {
                return null;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return null;
        }
    }
}
