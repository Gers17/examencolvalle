package com.example.examenandroid.ui.map;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.examenandroid.R;
import com.example.examenandroid.ui.base.BaseActivity;
import com.example.examenandroid.utils.AppConstants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapActivity extends BaseActivity implements OnMapReadyCallback, MapMVP.View {

    private GoogleMap mMap;
    @BindView(R.id.etNumbers)
    EditText etNumbers;
    @Inject
    MapMVP.Presenter presenter;

    private Boolean localizationPermission = false;
    private static final int PERMISSION_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUp();
    }

    @Override
    protected void setUp() {
        getComponent().inject(this);
        presenter.setView(this);
        ButterKnife.bind(this);
        getPermissionLocalizate();
    }

    @OnClick(R.id.btSend)
    public void generateRandomMarkers(){

        presenter.generateMarkers(Integer.parseInt(etNumbers.getText().toString().trim()),mMap.getCameraPosition().target);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (localizationPermission) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            } else {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);

                LatLng randomLatLng = new LatLng(19.673331, -99.205041);


                CameraPosition cameraPosition = new CameraPosition.Builder().target(randomLatLng).zoom(AppConstants.ZOOM_MAP).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    }

    private void startMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void updateMarkers(List<LatLng> positions) {
        for (LatLng position : positions){
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(position.latitude, position.longitude))
                    .anchor(0.5f, 0.5f)
            );
        }

    }

    @Override
    public void inputError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();

    }

    private void getPermissionLocalizate(){
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if(ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                localizationPermission = true;
                startMap();
            }else{
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_CODE);
            }
        }else{
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        localizationPermission = false;
        switch (requestCode) {
            case PERMISSION_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            localizationPermission = false;
                            return;
                        }
                    }
                    localizationPermission = true;
                    startMap();
                }
            }
        }
    }
}



