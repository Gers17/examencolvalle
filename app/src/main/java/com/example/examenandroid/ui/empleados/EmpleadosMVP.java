package com.example.examenandroid.ui.empleados;

import com.example.examenandroid.data.models.Empleados;

import java.util.List;

public interface EmpleadosMVP {

    interface Model{
        void saveEmpleados(List<Empleados> empleadosList);
        List<Empleados> getEmpleados();
    }

    interface View{
        void error(String message);
        void updateEmpleados(List<Empleados> empleadosList);
    }

    interface Presenter{
        void setView(View view);
        void loadEmpleados();
    }
}
