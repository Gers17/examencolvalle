package com.example.examenandroid.ui.webservice;

import com.example.examenandroid.data.models.Ubicaciones;
import com.example.examenandroid.data.models.request.RequestUbicaciones;
import com.example.examenandroid.data.network.DownloadService;
import com.example.examenandroid.data.network.NetwokAPI;
import com.google.gson.JsonObject;

import io.reactivex.Observable;
import okhttp3.ResponseBody;

public class WebServiceModel implements WebServiceMVP.Model {

    NetwokAPI netwokAPI;
    DownloadService downloadService;
    public WebServiceModel(NetwokAPI netwokAPI, DownloadService downloadService) {
        this.netwokAPI = netwokAPI;
        this.downloadService = downloadService;
    }

    @Override
    public Observable<Ubicaciones> getUbicaciones(RequestUbicaciones data) {
        return netwokAPI.getUbicaciones(data);
    }

    @Override
    public Observable<ResponseBody> downloadFile(String urlFile) {
        return downloadService.downloadFile(urlFile);
    }
}
