package com.example.examenandroid.ui.empleados;

import android.os.Bundle;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examenandroid.R;
import com.example.examenandroid.data.models.Empleados;
import com.example.examenandroid.ui.base.BaseActivity;
import com.example.examenandroid.ui.empleados.adapter.EmpleadosAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmpleadosActivity extends BaseActivity implements EmpleadosMVP.View{

    @Inject EmpleadosMVP.Presenter presenter;

    @Inject EmpleadosAdapter adapter;

    @BindView(R.id.rvEmpleados) RecyclerView rvEmpleados;

    @Override
    protected void setUp() {
        if(getComponent()!=null) {
            getComponent().inject(this);
            ButterKnife.bind(this);
            presenter.setView(this);
            rvEmpleados.setLayoutManager(new LinearLayoutManager(this));
            rvEmpleados.setNestedScrollingEnabled(false);
            rvEmpleados.setAdapter(adapter);
            presenter.loadEmpleados();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empleados);
        setUp();
    }

    @Override
    public void error(String message) {
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateEmpleados(List<Empleados> empleadosList) {
        adapter.addEmpleados(empleadosList);
        adapter.notifyDataSetChanged();
    }


}
