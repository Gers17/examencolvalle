package com.example.examenandroid.ui.thread;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.examenandroid.R;
import com.example.examenandroid.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ThreadActivity extends BaseActivity implements ThreadMVP.View {


    @Inject ThreadMVP.Presenter presenter;

    @BindView(R.id.container) LinearLayout lnContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
        setUp();
    }

    @Override
    protected void setUp() {
        getComponent().inject(this);
        ButterKnife.bind(this);
        presenter.setView(this);
        Button btn = presenter.createButton(lnContainer);
        btn.setOnClickListener(v -> {
            presenter.startTask();
        });

    }

    @Override
    public void taskCompleted(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }
}
