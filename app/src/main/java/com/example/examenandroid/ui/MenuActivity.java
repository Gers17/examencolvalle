package com.example.examenandroid.ui;

import android.content.Intent;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;

import com.example.examenandroid.R;
import com.example.examenandroid.ui.empleados.EmpleadosActivity;
import com.example.examenandroid.ui.map.MapActivity;
import com.example.examenandroid.ui.thread.ThreadActivity;
import com.example.examenandroid.ui.webservice.WebServiceActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MenuActivity extends AppCompatActivity {

    @OnClick(R.id.btn_map)
    public void openMap(){
        startActivity(new Intent(this, MapActivity.class));
    }

    @OnClick(R.id.btn_employees)
    public void openEmployees(){
        startActivity(new Intent(this, EmpleadosActivity.class));
    }

    @OnClick(R.id.btn_locations)
    public void openLocations(){
        startActivity(new Intent(this, WebServiceActivity.class));
    }

    @OnClick(R.id.btn_timer)
    public void openTimer(){
        startActivity(new Intent(this, ThreadActivity.class));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
    }
}
