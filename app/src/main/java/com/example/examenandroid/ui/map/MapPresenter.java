package com.example.examenandroid.ui.map;

import android.content.Context;

import com.example.examenandroid.R;
import com.example.examenandroid.utils.AppConstants;
import com.example.examenandroid.utils.AppUtils;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import butterknife.internal.Utils;

public class MapPresenter implements MapMVP.Presenter {
    private Context context;
    MapMVP.View view;

    public MapPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void setView(MapMVP.View view) {
        this.view = view;


    }

    @Override
    public void generateMarkers(int size, LatLng center) {
        if(size>0){
            List<LatLng> latLngList = new ArrayList<>();
            for (int i=0;i<size;i++){
                latLngList.add(AppUtils.getRandomLocation(center, AppConstants.RADIUS_MARKERS));
            }
            view.updateMarkers(latLngList);
        }else {
            view.inputError(context.getString(R.string.invalid_num));
        }

    }
}
