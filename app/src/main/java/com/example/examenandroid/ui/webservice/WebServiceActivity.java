package com.example.examenandroid.ui.webservice;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.examenandroid.R;
import com.example.examenandroid.ui.base.BaseActivity;
import com.example.examenandroid.ui.map.MapMVP;
import com.example.examenandroid.utils.AppConstants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebServiceActivity extends BaseActivity implements WebServiceMVP.View, OnMapReadyCallback {

    private GoogleMap mMap;

    @BindView(R.id.etNumbers) EditText etNumbers;
    @BindView(R.id.btSend) Button btSend;

    @Inject
    WebServiceMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUp();
    }

    @Override
    protected void setUp() {
        getComponent().inject(this);
        ButterKnife.bind(this);
        presenter.setView(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        checkWriteExternalMemoryPermission();
        btSend.setEnabled(false);
        etNumbers.setEnabled(false);


    }

    @Override
    public void updateMarkers(List<LatLng> positions) {
        runOnUiThread(new Runnable() {
            public void run() {

                if(positions!=null && positions.size()>0)
                    for (LatLng position : positions){
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(position.latitude, position.longitude))
                                .anchor(0.5f, 0.5f)
                        );
                    }

                CameraPosition cameraPosition = new CameraPosition.Builder().target(positions.get(0)).zoom(AppConstants.ZOOM_MAP).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

    }

    @Override
    public void error(String message) {
        runOnUiThread(new Runnable() {
            public void run() {

                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Pedir permisos para usar la ubicación
     */
    private void checkWriteExternalMemoryPermission(){
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        if(ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                presenter.getUbicaciones();
            }else{
                ActivityCompat.requestPermissions(this, permissions, 1);
            }
        }else{
            ActivityCompat.requestPermissions(this, permissions, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 1:{
                if(grantResults.length > 0){
                    for(int i = 0; i < grantResults.length; i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            return;
                        }
                    }
                    presenter.getUbicaciones();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
}
