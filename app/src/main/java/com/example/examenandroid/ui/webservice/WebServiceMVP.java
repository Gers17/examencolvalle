package com.example.examenandroid.ui.webservice;

import com.example.examenandroid.data.models.Ubicaciones;
import com.example.examenandroid.data.models.request.RequestUbicaciones;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;

public interface WebServiceMVP {

    interface Model{
        Observable<Ubicaciones> getUbicaciones(RequestUbicaciones data);
        Observable<ResponseBody> downloadFile(String urlFile);

    }

    interface View{
        void updateMarkers(List<LatLng> positions);
        void error(String message);
    }

    interface Presenter{
        void setView(View view);
        void getUbicaciones();
    }
}
