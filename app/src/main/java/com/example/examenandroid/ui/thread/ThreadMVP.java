package com.example.examenandroid.ui.thread;

import android.view.ViewGroup;
import android.widget.Button;

public interface ThreadMVP {

    interface View{
        void taskCompleted(String message);

    }

    interface Presenter{
        void setView(View view);
        void startTask();
        Button createButton(ViewGroup viewGroup);
    }
}
