package com.example.examenandroid.ui.empleados;

import com.example.examenandroid.data.models.Empleados;

import java.util.ArrayList;
import java.util.List;

public class EmpleadosPresenter implements EmpleadosMVP.Presenter {

    EmpleadosMVP.View view;
    EmpleadosMVP.Model model;


    public EmpleadosPresenter(EmpleadosMVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(EmpleadosMVP.View view) {
        this.view = view;
    }



    @Override
    public void loadEmpleados() {

        List<Empleados> employeeList = new ArrayList<>();
        employeeList.add(new Empleados("Miguel Cervantes","08-Dic-1990","Desarrollador"));
        employeeList.add(new Empleados("Juan Morales","03-Jul-1990","Desarrollador"));
        employeeList.add(new Empleados("Roberto Méndez","14-Oct-1990","Desarrollador"));
        employeeList.add(new Empleados("Miguel Cuevas","08-Dic-1990","Desarrollador"));
        model.saveEmpleados(employeeList);
        view.updateEmpleados(model.getEmpleados());

    }


}
