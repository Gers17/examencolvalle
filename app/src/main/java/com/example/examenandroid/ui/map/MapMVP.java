package com.example.examenandroid.ui.map;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public interface MapMVP {

    interface Model{

    }

    interface View{
        void updateMarkers(List<LatLng> positions);
        void inputError(String error);
    }

    interface Presenter{
    void setView(View view);
    void generateMarkers(int size,LatLng center);


    }
}
