package com.example.examenandroid.ui.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.examenandroid.di.component.ActivityComponent;
import com.example.examenandroid.di.component.DaggerActivityComponent;
import com.example.examenandroid.di.module.ActivityModule;

public abstract class BaseActivity extends AppCompatActivity {
    private ActivityComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected abstract void setUp();

    protected ActivityComponent getComponent(){
        if (component==null){
            component = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .build();
        }

        return component;
    }

}
