package com.example.examenandroid.ui.empleados;

import com.example.examenandroid.data.db.EmpleadosHelper;
import com.example.examenandroid.data.models.Empleados;

import java.util.List;

public class EmpleadosModel implements EmpleadosMVP.Model {
    private EmpleadosHelper empleadosHelper;

    public EmpleadosModel(EmpleadosHelper empleadosHelper) {
        this.empleadosHelper = empleadosHelper;
    }

    @Override
    public void saveEmpleados(List<Empleados> empleadosList) {
        empleadosHelper.saveEmpleados(empleadosList);

    }

    @Override
    public List<Empleados> getEmpleados() {
        return empleadosHelper.getEmpleados();
    }
}
