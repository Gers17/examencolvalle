package com.example.examenandroid.ui.empleados.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examenandroid.R;
import com.example.examenandroid.data.models.Empleados;

import java.util.List;


public class EmpleadosAdapter extends RecyclerView.Adapter<EmpleadosAdapter.EmpleadosViewHolder> {
    private List<Empleados> empleados;

    public EmpleadosAdapter(List<Empleados> empleados) {
        this.empleados = empleados;
    }

    public void addEmpleados(List<Empleados> empleados){
        this.empleados =empleados;
    }

    @NonNull
    @Override
    public EmpleadosViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_empleados,viewGroup,false);
        return new EmpleadosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmpleadosViewHolder empleadosViewHolder, int position) {
        if(empleados!=null){
            empleadosViewHolder.setEmpleados(position);
        }
    }


    @Override
    public int getItemCount() {
        if(empleados!=null){
            return empleados.size();
        }else {
            return 0;
        }
    }

    class EmpleadosViewHolder extends RecyclerView.ViewHolder{

        TextView tvName;
        TextView tvBithDay;
        TextView tvOccupation;

        public EmpleadosViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvBithDay = itemView.findViewById(R.id.tv_bith_day);
            tvOccupation = itemView.findViewById(R.id.tv_occupation);
        }

        public void setEmpleados(int position){
            tvName.setText(String.valueOf(empleados.get(position).getName()));
            tvBithDay.setText(String.valueOf(empleados.get(position).getBirthDate()));
            tvOccupation.setText(empleados.get(position).getOccupation());
        }

    }
}
