package com.example.examenandroid.ui.webservice;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.CheckResult;

import com.example.examenandroid.R;
import com.example.examenandroid.data.models.request.RequestUbicaciones;
import com.example.examenandroid.utils.AppConstants;
import com.example.examenandroid.utils.AppUtils;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class WebServicePresenter implements WebServiceMVP.Presenter {

    private Context context;

    WebServiceMVP.View view;
    WebServiceMVP.Model model;

    public WebServicePresenter(WebServiceMVP.Model model, Context context) {
        this.context = context;
        this.model = model;
    }

    @Override
    public void setView(WebServiceMVP.View view) {
        this.view = view;

    }

    @SuppressLint("CheckResult")
    @Override
    public void getUbicaciones() {
        RequestUbicaciones requestUbicaciones = new RequestUbicaciones();
        requestUbicaciones.setServiceId(55);
        requestUbicaciones.setUserId(12984);
        model.getUbicaciones(requestUbicaciones)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(result->
                    model.downloadFile(result.getValueResponse())
                )
                .subscribe(
                        responseBody ->{
                            descargaArchvo(responseBody);
                        },
                        error -> {
                            view.error(error.getMessage());
                        }

                );

    }

    private void descargaArchvo(ResponseBody responseBody){

        File file = AppUtils.writeResponseBodyToDisk(responseBody,context, AppConstants.ZIP);
        if(file!=null){
            try {
                final ZipFile zipFile = new ZipFile(file);

                Enumeration<? extends ZipEntry> e = zipFile.entries();
                ZipEntry entry = e.nextElement();

                // obtenemos el primer archivo
                String entryName = entry.getName();

                InputStream input = zipFile.getInputStream(zipFile.getEntry(entryName));
                BufferedReader br = new BufferedReader(new InputStreamReader(input, "UTF-8"));

                StringBuilder sb = new StringBuilder();

                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                JSONObject jsonObject = new JSONObject(sb.toString());

                JSONArray arrayData = new JSONObject(sb.toString()).getJSONArray("data");
                JSONArray arrayUbicaciones = arrayData.getJSONObject(0).getJSONArray("UBICACIONES");

                List<LatLng> latLngList = new ArrayList<>();

                for (int i=0; i<arrayUbicaciones.length();i++){
                    JSONObject item = arrayUbicaciones.getJSONObject(i);
                    latLngList.add(new LatLng(item.getDouble("latitud"),item.getDouble("longitud")));
                }
                if(latLngList!=null && latLngList.size()>0){
                    view.updateMarkers(latLngList);
                }else {
                    view.error(context.getString(R.string.without_locations));
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            view.error("Error al descomprimir zip");
        }
    }



    }

