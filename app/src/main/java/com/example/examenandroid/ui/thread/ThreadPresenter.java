package com.example.examenandroid.ui.thread;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ThreadPresenter implements ThreadMVP.Presenter {
    Context context;

    ThreadMVP.View view;

    public ThreadPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void setView(ThreadMVP.View view) {
    this.view = view;
    }

    @Override
    public void startTask() {
        Observable.timer(5, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        it -> view.taskCompleted("Tarea Finalizada")
                );

    }

    @Override
    public Button createButton(ViewGroup viewGroup) {
        Button btnTimer = new Button(context);
        btnTimer.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        btnTimer.setText("Tiempo");
        viewGroup.addView(btnTimer);
        return btnTimer;
    }
}
